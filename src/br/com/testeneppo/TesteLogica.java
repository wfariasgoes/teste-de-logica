package br.com.testeneppo;

import java.util.Scanner;

public class TesteLogica {
    public static void main(String[] args) {

        Scanner ent = new Scanner(System.in);

        int numTeste;


        System.out.println("informe o teste que deseja fazer[1, 2, 3]");
        numTeste = ent.nextInt();

        switch (numTeste) {
            case 1:
                formulaMisteriosa();
                break;
            case 2:
                maquinaDoTempo();
                break;
            case 3:
                garconsModernos();
                break;

        }


    }

    /*
        teste 1
    */
    private static void formulaMisteriosa() {
        Long num;
        Long saida = 0L;

        Scanner entrada1 = new Scanner(System.in);
        System.out.println("Digite o número válido");
        num = entrada1.nextLong();
        while (num != -1) {
            if (num == 1) {
                for (Long i = num; i <= 11; i++) {
                    saida = num++;
                }
                System.out.println(saida);

            } else if (num == 11) {
                for (Long i = num; i <= 21; i++) {
                    saida = num++;
                }
                System.out.println(saida);

            } else if (num == 21) {
                for (Long i = num; i <= 1211; i++) {
                    saida = num++;
                }
                System.out.println(saida);

            } else if (num == 1211) {
                for (Long i = num; i <= 111221; i++) {
                    saida = num++;
                }


                System.out.println(saida);

            } else if (num == 111221) {
                for (Long i = num; i <= 312211; i++) {
                    saida = num++;
                }
                System.out.println(saida);
            } else {
                System.out.println("Numero inválido");
            }
            System.out.println("Digite o número válido ou (-1) para sair");
            num = entrada1.nextLong();
        }

    }

    /*
        teste 2
    */
    private static void maquinaDoTempo() {
        int ano;
        int dia;
        int mes;
        boolean bisexto;
        Scanner ent = new Scanner(System.in);

        System.out.println("Digite o dia:");
        dia = ent.nextInt();

        while (dia != -1) {
            System.out.println("Digite o mês:");
            mes = ent.nextInt();

            System.out.println("Digite o ano:");
            ano = ent.nextInt();

            if (ano % 400 == 0) {
                bisexto = true;
                validacao(dia, mes, bisexto);

            } else if ((ano % 4 == 0) && (ano % 100 != 0)) {
                bisexto = true;
                validacao(dia, mes, bisexto);

            } else {
                bisexto = false;
                validacao(dia, mes, bisexto);

            }

            System.out.println("\nDigite o dia ou (-1) para sair:");
            dia = ent.nextInt();
        }
    }

    private static void validacao(int dia, int mes, boolean bisexto) {

        if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12) {

            if (dia <= 31) {

                System.out.println(true);

            }

        }

        if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
            if (dia <= 30) {
                System.out.println(true);
            } else {
                System.out.println(false);
            }
        }
        if (mes == 2 && bisexto == true) {
            if (dia <= 29) {
                System.out.println(true);
            }
        }
        if (mes == 2 && bisexto == false) {
            if (dia >= 29) {
                System.out.println(false);

            } else {

                System.out.println(true);

            }

        }

    }

    /*
       teste 3
   */
    private static void garconsModernos() {
        Scanner entrada = new Scanner(System.in);
        int inteiro;
        int numDigitado;
        String saida = "";

        System.out.print("Digite uma número entre 1 e 3999:");
        inteiro = entrada.nextInt();

        numDigitado = inteiro;
        if (inteiro < 4000) {
            while (inteiro >= 1000) {
                saida += "M";
                inteiro = inteiro - 1000;
            }

            while (inteiro >= 900) {
                saida += "CM";
                inteiro = inteiro - 900;
            }


            while (inteiro >= 500) {
                saida += "D";
                inteiro = inteiro - 500;
            }


            while (inteiro >= 400) {
                saida += "CD";
                inteiro = inteiro - 400;
            }

            while (inteiro >= 100) {
                saida += "C";
                inteiro = inteiro - 100;
            }


            while (inteiro >= 90) {
                saida += "XC";
                inteiro = inteiro - 90;
            }

            while (inteiro >= 50) {
                saida += "L";
                inteiro = inteiro - 50;
            }

            while (inteiro >= 40) {
                saida += "XL";
                inteiro = inteiro - 40;
            }


            while (inteiro >= 10) {
                saida += "X";
                inteiro = inteiro - 10;
            }

            while (inteiro >= 9) {
                saida += "IX";
                inteiro = inteiro - 9;
            }

            while (inteiro >= 5) {
                saida += "V";
                inteiro -= 5;
            }

            while (inteiro >= 4) {
                saida += "IV";
                inteiro -= 4;
            }

            while (inteiro >= 1) {
                saida += "I";
                inteiro -= 1;
            }
            System.out.println("O Algarismo correspondente a " + numDigitado + " é " + saida);
        } else {
            System.out.print("inteiro Inválido!");
        }


    }


}
